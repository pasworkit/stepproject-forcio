<h1>Привіт! Вітаємо на сторінках нашого проекту:)))</h1> 

<h3>Виконавці проекту:Паришкура Олександр; Ялоза Євген.</h3>


<p>Паришкура Олександр: верстка хедеру(включаючи випадаюче меню при малій роздільній здатності екрана), верстка секції People Are Talking About Fork.</p>
<p>Ялоза Євген: верстка секцій: Revolutionary Editor, Here is what you get, Fork Subscription Pricing</p>

<h3>Список використаних технологій:</h3>
<ul>
<li>HTML</li>
<li>CSS</li>
<li>JavaScript</li>
<li>Git</li>
<li>NodeJS</li>
<li>NPM, Gulp</li>
<li>SCSS</li>
</ul>